package ru.kuzin.tm.api.controller;

public interface ITaskController {

    void showTasks();

    void createTasks();

    void clearTasks();

    void removeTaskById();

    void removeTaskByIndex();

    void showTaskById();

    void showTaskByIndex();

    void showTaskByProjectId();

    void updateTaskByIndex();

    void updateTaskById();

    void startTaskById();

    void startTaskByIndex();

    void changeTaskStatusById();

    void changeTaskStatusByIndex();

    void completeTaskById();

    void completeTaskByIndex();

}