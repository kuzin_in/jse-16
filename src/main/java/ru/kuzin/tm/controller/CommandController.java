package ru.kuzin.tm.controller;

import ru.kuzin.tm.api.controller.ICommandController;
import ru.kuzin.tm.api.service.ICommandService;
import ru.kuzin.tm.model.Command;

import static ru.kuzin.tm.util.FormatUtil.formatBytes;

public final class CommandController implements ICommandController {

    private final ICommandService commandService;

    public CommandController(final ICommandService commandService) {
        this.commandService = commandService;
    }

    @Override
    public void showErrorCommand() {
        System.err.println("\n[ERROR]");
        System.err.println("Current command is not correct!");
        System.exit(1);
    }

    @Override
    public void showSystemInfo() {
        System.out.println("\n[SYSTEM INFO]");
        final int processorCount = Runtime.getRuntime().availableProcessors();
        final long totalMemory = Runtime.getRuntime().totalMemory();
        final long maxMemory = Runtime.getRuntime().maxMemory();
        final long freeMemory = Runtime.getRuntime().freeMemory();
        final long usedMemory = totalMemory - freeMemory;

        System.out.println("PROCESSORS: " + processorCount);
        System.out.println("MAX MEMORY: " + formatBytes(maxMemory));
        System.out.println("TOTAL MEMORY: " + formatBytes(totalMemory));
        System.out.println("FREE MEMORY: " + formatBytes(freeMemory));
        System.out.println("USED MEMORY: " + formatBytes(usedMemory));
    }

    @Override
    public void showErrorArgument() {
        System.err.println("\n[ERROR]");
        System.err.println("Current argument is not correct!");
    }

    @Override
    public void showVersion() {
        System.out.println("\n[VERSION]");
        System.out.println("1.11.0");
    }

    @Override
    public void showAbout() {
        System.out.println("\n[ABOUT]");
        System.out.println("Name: Kuzin Igor");
        System.out.println("E-mail: garbir9@mail.ru");
    }

    @Override
    public void showHelp() {
        System.out.println("\n[HELP]");
        for (Command command : commandService.getCommands()) System.out.println(command);
    }

}