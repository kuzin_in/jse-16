package ru.kuzin.tm.component;

import ru.kuzin.tm.api.controller.ICommandController;
import ru.kuzin.tm.api.controller.IProjectController;
import ru.kuzin.tm.api.controller.IProjectTaskController;
import ru.kuzin.tm.api.controller.ITaskController;
import ru.kuzin.tm.api.repository.ICommandRepository;
import ru.kuzin.tm.api.repository.IProjectRepository;
import ru.kuzin.tm.api.repository.ITaskRepository;
import ru.kuzin.tm.api.service.*;
import ru.kuzin.tm.constant.ArgumentConst;
import ru.kuzin.tm.constant.CommandConst;
import ru.kuzin.tm.controller.CommandController;
import ru.kuzin.tm.controller.ProjectController;
import ru.kuzin.tm.controller.ProjectTaskController;
import ru.kuzin.tm.controller.TaskController;
import ru.kuzin.tm.enumerated.Status;
import ru.kuzin.tm.exception.system.ArgumentNotSupportedException;
import ru.kuzin.tm.exception.system.CommandNotSupportedException;
import ru.kuzin.tm.model.Project;
import ru.kuzin.tm.repository.CommandRepository;
import ru.kuzin.tm.repository.ProjectRepository;
import ru.kuzin.tm.repository.TaskRepository;
import ru.kuzin.tm.service.*;
import ru.kuzin.tm.util.TerminalUtil;

public final class Bootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ITaskController taskController = new TaskController(taskService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final ILoggerService loggerService = new LoggerService();

    private void initDemoData() {
        projectService.add(new Project("PROJECT 1", Status.IN_PROGRESS));
        projectService.add(new Project("PROJECT 3", Status.COMPLETED));
        projectService.add(new Project("PROJECT 2", Status.NOT_STARTED));

        taskService.create("Task 1");
        taskService.create("Task 3");
        taskService.create("Task 4");
        taskService.create("Task 2");
    }

    private void processCommand() {
        while (!Thread.currentThread().isInterrupted()) {
            try {
                System.out.println("\nENTER COMMAND: ");
                final String command = TerminalUtil.nextLine();
                processCommand(command);
                System.out.println("[OK]");
                loggerService.command(command);
            } catch (final Exception e) {
                loggerService.error(e);
                System.out.println("[FAIL]");
            }
        }
    }

    private boolean processArgument(final String[] arguments) {
        if (arguments == null || arguments.length < 1) return false;
        final String arg = arguments[0];
        processArgument(arg);
        return true;
    }

    private void exit() {
        System.exit(0);
    }

    private void initLogger() {
        loggerService.info("*** WELCOME TO TASK MANAGER ***");
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
                loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
            }
        });
    }

    private void processCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case CommandConst.VERSION:
                commandController.showVersion();
                break;
            case CommandConst.ABOUT:
                commandController.showAbout();
                break;
            case CommandConst.INFO:
                commandController.showSystemInfo();
                break;
            case CommandConst.HELP:
                commandController.showHelp();
                break;
            case CommandConst.PROJECT_LIST:
                projectController.showProjects();
                break;
            case CommandConst.PROJECT_CREATE:
                projectController.createProjects();
                break;
            case CommandConst.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case CommandConst.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case CommandConst.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case CommandConst.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case CommandConst.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case CommandConst.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case CommandConst.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case CommandConst.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case CommandConst.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case CommandConst.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case CommandConst.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case CommandConst.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case CommandConst.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case CommandConst.TASK_LIST:
                taskController.showTasks();
                break;
            case CommandConst.TASK_CREATE:
                taskController.createTasks();
                break;
            case CommandConst.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case CommandConst.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case CommandConst.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case CommandConst.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case CommandConst.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case CommandConst.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case CommandConst.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case CommandConst.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case CommandConst.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case CommandConst.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case CommandConst.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case CommandConst.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case CommandConst.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case CommandConst.TASK_SHOW_BY_PROJECT_ID:
                taskController.showTaskByProjectId();
                break;
            case CommandConst.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case CommandConst.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;

            case CommandConst.EXIT:
                exit();
                break;
            default:
                throw new CommandNotSupportedException(command);
        }
    }

    private void processArgument(final String argument) {
        switch (argument) {
            case ArgumentConst.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConst.ABOUT:
                commandController.showAbout();
                break;
            case ArgumentConst.INFO:
                commandController.showSystemInfo();
                break;
            case ArgumentConst.HELP:
                commandController.showHelp();
                break;
            default:
                throw new ArgumentNotSupportedException(argument);
        }
    }

    public void run(final String[] args) {
        processArgument(args);
        if (processArgument(args)) System.exit(0);
        initDemoData();
        initLogger();
        processCommand();
    }

}